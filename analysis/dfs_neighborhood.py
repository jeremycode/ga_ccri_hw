import time

### main start:
def main(grid, n):
    ROWS = len(grid)
    COLUMNS = len(grid[0])
    directions = [[0, 1], [1, 0], [0, -1], [-1, 0]]
    seen = set()
    counted = set()

    def check_valid(curr_row, curr_col):
        return 0 <= curr_row < ROWS and 0 <= curr_col < COLUMNS

    def distance(curr_row, curr_col, orig_row, orig_col):
        return abs(curr_row - orig_row) + abs(curr_col - orig_col)

    ### dfs start:
    def dfs(new_row, new_col, orig_row, orig_col):
        dist = distance(new_row, new_col, orig_row, orig_col)
        # Base Case:
        if not check_valid(new_row, new_col) or dist > n or (new_row, new_col) in seen:
            return

        # Logic:
        seen.add((new_row, new_col))
        counted.add((new_row, new_col))
        output_cells.append((new_row, new_col))

        # Recursive Calls:
        for d_row, d_col in directions:
            this_row, this_col = new_row + d_row, new_col + d_col
            dfs(this_row, this_col, orig_row, orig_col)

    ### dfs end:

    # add all the positive to original cell list:
    orig_pos_cells = set()
    output_cells = []
    for row in range(ROWS):
        for col in range(COLUMNS):
            if grid[row][col] > 0:
                orig_pos_cells.add((row, col))

    # Explore directions on original cells:
    for orig_row, orig_col in orig_pos_cells:
        seen.clear()
        dfs(orig_row, orig_col, orig_row, orig_col)

    return len(counted)

### main end

# grid = [
#     [1, 0, 0, 0, 1],
#     [0, 0, 0, 0, 0],
#     [0, 0, 1, 0, 0],
#     [0, 0, 0, 0, 0],
#     [1, 0, 0, 0, 1]
# ]
# n = 1

# output = main(grid, n)
# print(f"number of cells within {n}: {output}")

grid = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
]
n = 2

start_time = time.time()
output = main(grid, n)
end_time = time.time()
sparse_time = end_time - start_time
print(f"DFS: number of cells within {n}: {output}")
print(f"DFS: time to run sparse grid: {sparse_time}")

grid = [
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0],
    [0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0]
]
n = 2
sum_grid = sum([sum(row) for row in grid])
print("Num pos in grid: ", sum_grid)

start_time = time.time()
output = main(grid, n)
end_time = time.time()
dense_time = end_time - start_time
print(f"DFS: number of cells within {n}: {output}")
print(f"DFS: time to run dense grid: {dense_time}")
print(f"DFS: ratio: dense/sparse {dense_time/sparse_time}")