from collections import deque 
import time

def main(grid, n):
    ROWS = len(grid)
    COLUMNS = len(grid[0])
    directions = [[0,1], [1,0], [0,-1], [-1,0]]
    seen = set()

    def check_valid(curr_row, curr_col):
        return 0 <= curr_row < ROWS and 0 <= curr_col < COLUMNS
    def distance(curr_row, curr_col, orig_row, orig_col):
        return abs(curr_row - orig_row) + abs(curr_col - orig_col)
    
    # Initialization:
    orig_pos_cells = set()
    output_cells = set()
    for row in range(ROWS):
        for column in range(COLUMNS):
            if grid[row][column] > 0:
                orig_pos_cells.add((row, column))

    cell_count = 0

    for orig_pos_cell in orig_pos_cells:
        orig_row, orig_col = orig_pos_cell[0], orig_pos_cell[1]
        queue = deque([(orig_row, orig_col)])
        seen.clear()
        seen.add((orig_row, orig_col))
        output_cells.add((orig_row, orig_col))
        # Loop through while the queue is not empty:
        while (queue):
            level_len = len(queue)
            for level in range(level_len):
                curr_row, curr_col = queue.popleft()
                # Explore directions:
                for d_row, d_col in directions:
                    new_row, new_col = curr_row + d_row, curr_col + d_col
                    dist = distance(new_row, new_col, orig_row, orig_col)
                    if dist <= n and (new_row, new_col) not in seen: # and check_valid(new_row, new_col):
                        # if this new_row/new_col is:
                        # valid 
                        # within the distance from the original
                        # not already been seen
                        seen.add((new_row, new_col))
                        queue.append((new_row, new_col))
                        output_cells.add((new_row % ROWS, new_col % COLUMNS))
    
    # # wrap values around:
    # final_cells = set()
    # for this_row, this_col in output_cells:
    #     final_cells.add((this_row % ROWS, this_col % COLUMNS))
        
    return len(output_cells)

grid = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
]
n = 2

start_time = time.time()
output = main(grid, n)
end_time = time.time()
sparse_time = end_time - start_time
print(f"BFS: number of cells within {n}: {output}")
print(f"BFS: time to run sparse grid: {sparse_time}")

grid = [
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0],
    [0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0]
]
n = 2
sum_grid = sum([sum(row) for row in grid])
print("Num pos in grid: ", sum_grid)

start_time = time.time()
output = main(grid, n)
end_time = time.time()
dense_time = end_time - start_time
print(f"BFS: number of cells within {n}: {output}")
print(f"BFS: time to run dense grid: {dense_time}")
print(f"BFS: ratio: dense/sparse {dense_time/sparse_time}")
