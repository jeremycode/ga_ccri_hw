#include "gtest/gtest.h"
#include "../src/dfs_neighborhood/dfs_neighborhood.h"
#include <iostream>

// Define a test fixture class
class ReachableCellsTestDFS : public ::testing::Test
{
protected:
};

TEST_F(ReachableCellsTestDFS, EmptyGrid)
{
    // Test case with an empty grid
    std::vector<std::vector<int>> grid = {};
    int n = 2;
    int expected_output = 0;

    int actual_output = count_reachable_cells(grid, n);

    EXPECT_EQ(expected_output, actual_output);
}

TEST_F(ReachableCellsTestDFS, SingleCell)
{
    // Test case with a single positive cell
    std::vector<std::vector<int>> grid = {{1}};
    int n = 2;
    int expected_output = 1;

    int actual_output = count_reachable_cells(grid, n);

    EXPECT_EQ(expected_output, actual_output);
}

TEST_F(ReachableCellsTestDFS, AllCellReach)
{
    // Test case with a cell exceeding distance limit
    std::vector<std::vector<int>> grid = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
    int n = 1;
    int expected_output = 9;

    int actual_output = count_reachable_cells(grid, n);

    EXPECT_EQ(expected_output, actual_output);
}

TEST_F(ReachableCellsTestDFS, MultipleCells)
{
    // Test case with multiple positive cells
    std::vector<std::vector<int>> grid = {{1, 0, 1}, {0, 0, 0}, {1, 0, 1}};
    int n = 1;
    int expected_output = 8;

    int actual_output = count_reachable_cells(grid, n);

    EXPECT_EQ(expected_output, actual_output);
}
