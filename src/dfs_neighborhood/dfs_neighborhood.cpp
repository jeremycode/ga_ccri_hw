#include "dfs_neighborhood.h"

bool check_valid(int curr_row, int curr_col, int rows, int cols)
{
    return 0 <= curr_row && curr_row < rows && 0 <= curr_col && curr_col < cols;
}

int distance(int curr_row, int curr_col, int orig_row, int orig_col)
{
    return abs(curr_row - orig_row) + abs(curr_col - orig_col);
}

void dfs(vector<vector<int>> &grid, set<pair<int, int>> &seen,
         set<pair<int, int>> &counted, vector<pair<int, int>> &output_cells,
         int new_row, int new_col, int orig_row, int orig_col, int n)
{
    int dist = distance(new_row, new_col, orig_row, orig_col);

    // Base Case:
    if (!check_valid(new_row, new_col, grid.size(), grid[0].size()) || dist > n || seen.count({new_row, new_col}))
    {
        return;
    }

    // Logic:
    seen.insert({new_row, new_col});
    counted.insert({new_row, new_col});
    output_cells.push_back({new_row, new_col});

    // Recursive Calls:
    static const int directions[][2] = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
    for (int i = 0; i < 4; ++i)
    {
        int d_row = directions[i][0];
        int d_col = directions[i][1];
        dfs(grid, seen, counted, output_cells, new_row + d_row, new_col + d_col, orig_row, orig_col, n);
    }
}

int count_reachable_cells(vector<vector<int>> &grid, int n)
{
    int rows = grid.size(), cols = grid[0].size();
    set<pair<int, int>> seen, counted;
    vector<pair<int, int>> output_cells;

    // Get initial positive cells:
    set<pair<int, int>> orig_pos_cells;
    for (int row = 0; row < rows; ++row)
    {
        for (int col = 0; col < cols; ++col)
        {
            if (grid[row][col] > 0)
            {
                orig_pos_cells.insert({row, col});
            }
        }
    }

    // Explore directions from original cells:
    for (const auto &cell : orig_pos_cells)
    {
        seen.clear(); // Reset for each starting cell
        dfs(grid, seen, counted, output_cells, cell.first, cell.second, cell.first, cell.second, n);
    }

    return counted.size();
}
