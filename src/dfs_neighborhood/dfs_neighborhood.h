#ifndef REACHABLE_CELLS_H
#define REACHABLE_CELLS_H

#include <iostream>
#include <set>
#include <vector>

using namespace std;

bool check_valid(int curr_row, int curr_col, int rows, int cols);

int distance(int curr_row, int curr_col, int orig_row, int orig_col);

void dfs(vector<vector<int>> &grid, set<pair<int, int>> &seen,
         set<pair<int, int>> &counted, vector<pair<int, int>> &output_cells,
         int new_row, int new_col, int orig_row, int orig_col, int n);

int count_reachable_cells(vector<vector<int>> &grid, int n);

#endif
