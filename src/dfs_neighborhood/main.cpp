#include "dfs_neighborhood.h"
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<vector<int>> grid = {
        {1, 0, 0, 0, 1},
        {0, 0, 0, 0, 0},
        {0, 0, 1, 0, 0},
        {0, 0, 0, 0, 0},
        {1, 0, 0, 0, 1}};
    int n = 1;

    int output = count_reachable_cells(grid, n);
    cout << "number of cells within " << n << ": " << output << endl;

    return 0;
}
