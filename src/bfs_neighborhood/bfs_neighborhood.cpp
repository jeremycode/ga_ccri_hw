#include "bfs_neighborhood.h"

bool check_valid(int curr_row, int curr_col, int rows, int cols)
{
    return 0 <= curr_row && curr_row < rows && 0 <= curr_col && curr_col < cols;
}

int distance(int curr_row, int curr_col, int orig_row, int orig_col)
{
    return abs(curr_row - orig_row) + abs(curr_col - orig_col);
}

int count_reachable_cells(vector<vector<int>> &grid, int n)
{
    int rows = grid.size(), cols = grid[0].size();
    set<pair<int, int>> seen;
    set<pair<int, int>> output_cells;
    set<pair<int, int>> orig_pos_cells;

    for (int row = 0; row < rows; ++row)
    {
        for (int col = 0; col < cols; ++col)
        {
            if (grid[row][col] > 0)
            {
                orig_pos_cells.insert({row, col});
            }
        }
    }

    for (const pair<int, int> &orig_cell : orig_pos_cells)
    {
        int orig_row = orig_cell.first;
        int orig_col = orig_cell.second;
        queue<pair<int, int>> queue;
        queue.push({orig_row, orig_col});

        while (!queue.empty())
        {
            int level_len = queue.size();
            for (int level = 0; level < level_len; ++level)
            {
                pair<int, int> curr_cell = queue.front();
                queue.pop();

                int curr_row = curr_cell.first;
                int curr_col = curr_cell.second;

                // Explore directions
                static const int directions[][2] = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
                for (int i = 0; i < 4; ++i)
                {
                    int d_row = directions[i][0];
                    int d_col = directions[i][1];
                    int new_row = curr_row + d_row;
                    int new_col = curr_col + d_col;

                    if (check_valid(new_row, new_col, rows, cols) &&
                        distance(new_row, new_col, orig_row, orig_col) <= n &&
                        seen.count({new_row, new_col}) == 0)
                    {
                        seen.insert({new_row, new_col});
                        queue.push({new_row, new_col});
                        output_cells.insert({new_row, new_col});
                    }
                }
            }
        }
    }

    return output_cells.size();
}
