#ifndef REACHABLE_CELLS_H
#define REACHABLE_CELLS_H

#include <iostream>
#include <set>
#include <queue>
#include <vector>
#include <limits> // For numeric_limits

using namespace std;

bool check_valid(int curr_row, int curr_col, int rows, int cols);

int distance(int curr_row, int curr_col, int orig_row, int orig_col);

int count_reachable_cells(vector<vector<int>> &grid, int n);

#endif
